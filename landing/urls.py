from . import views
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('tours/get', views.get_tours),
    path('comments/get', views.get_comments),
    path('details/submit', views.submit_details),
    path('email/submit', views.submit_email),
    path('gallery/get', views.get_gallery),
    path('about_us/get', views.get_aboutus),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
