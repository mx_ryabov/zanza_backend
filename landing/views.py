from django.shortcuts import render
from django.core import serializers
import json
from django.http import JsonResponse
from .models import Tour, Comment, UserDetails, Gallery, AboutUsPictures
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def submit_details(req):
    data = json.loads(req.body)
    
    details = UserDetails(
        name=data.get('name'),
        email=data.get('email'),
        phone_number=data.get('phone_number'),
        isAgree=data.get('isAgree'),
        test_result=data.get('test_results')
    )
    details.save()
    
    return JsonResponse({"success": True})

@csrf_exempt
def submit_email(req):
    data = json.loads(req.body)
    
    details = UserDetails(
        email=data.get('email'),
        isAgree=True,
        test_result=data.get('test_results')
    )
    details.save()
    
    return JsonResponse({"success": True})

def get_aboutus(req):
    res = AboutUsPictures.objects.all()
    return JsonResponse({"res": serializeToJson(res)})

def get_tours(req):
    res = []
    if req.GET.get('type') in ['au', 'nz']:
        res = Tour.objects.filter(type_page=req.GET.get('type'))
    else:
        res = Tour.objects.filter(type_page="main")
    return JsonResponse({"res": serializeToJson(res)})


def get_comments(req):
    res = Comment.objects.filter(is_main_page=req.GET.get('is_main_page'))
    return JsonResponse({"res": serializeToJson(res)})

def get_gallery(req):
    res = Gallery.objects.all()
    photos = serializeToJson(res)
    for photo in photos:
        photo['width'] = 1
        photo['height'] = 1
        photo['src'] = '/uploads/' + photo['photo']
        photo['preview'] = photo['preview']

    return JsonResponse({"res": photos})

def serializeToJson(obj):
    obj = json.loads(serializers.serialize('json', obj))
    new_obj = []
    
    for el in obj:
        pk = el['pk']
        el = el['fields']
        el['pk'] = pk
        new_obj.append(el)

    return new_obj
